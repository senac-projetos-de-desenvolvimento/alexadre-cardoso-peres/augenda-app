# Project Augenda

## Description
Projeto com o intuito de ajudar tutores de pets no seu coditiano.

![Alt Text](https://media.discordapp.net/attachments/690660274755076136/980700941223346176/Screenshot_20220530-021957_Expo_Go.jpg?width=591&height=1247)

## Prerequisites

:white_check_mark: - Node.js

:white_check_mark: - React Native

:white_check_mark: - XCode | Iphone Run *or* - Android Studio | Android Run

:white_check_mark: - Expo | Native Cellphone Run

## Quick Start

```
  npm install --global expo-cli | Install Expo
   
  git clone | Clone This Repo
  
  cd Augenda
  
  yarn start
  
  "a" for ANDROID | "i" for IOS | QRcode for your Cellphone
```



## Libraries

:white_check_mark: - Expo: https://docs.expo.io/

:white_check_mark: - Styled Components: https://styled-components.com

:white_check_mark: - Zustand: https://github.com/pmndrs/zustand

:white_check_mark: - Expo Fonts: https://docs.expo.io/guides/using-custom-fonts

:white_check_mark: - AsyncStorage: https://docs.expo.io/versions/latest/sdk/async-storage

:white_check_mark: - AsyncStorage(New Version): https://react-native-async-storage.github.io/async-storage/docs/install

:white_check_mark: - React Navigation: https://reactnavigation.org/

:white_check_mark: - Axios: https://github.com/axios/axios

:white_check_mark: - Expo Camera: https://docs.expo.dev/versions/latest/sdk/camera/

:white_check_mark: - Expo ImagePicker: https://docs.expo.dev/versions/latest/sdk/imagepicker/

:white_check_mark: - MUI React UI tools: https://mui.com/pt/

:white_check_mark: - React gesture handler: https://docs.swmansion.com/react-native-gesture-handler/docs/

:white_check_mark: - React native maps: https://github.com/react-native-maps/react-native-maps

:white_check_mark: - React Native Reanimated: https://docs.swmansion.com/react-native-reanimated/

:white_check_mark: - React safe area context: https://www.npmjs.com/package/react-native-safe-area-context

:white_check_mark: - Babel: https://babeljs.io

:white_check_mark: - Cypress: https://www.cypress.io