import React from 'react';
import { StyleSheet, View } from 'react-native';

export default function CardBack(props){
    return(
        <View style={styles.card}>
            <View style={styles.cardContent}>
                { props.children }
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    card:{
        borderRadius: 6,
        marginBottom: 100,
        elevation: 3,
        backgroundColor: '#d5d5d57a',
        marginHorizontal: 4,
        marginVertical: 6,
        width: '350px',
    },
    cardContent:{
        marginHorizontal: 18,
        marginVertical: 10,
    },
})