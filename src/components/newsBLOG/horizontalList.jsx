import React from 'react';
import { View, StyleSheet, FlatList } from 'react-native'
import SmallerCard from './smallCard';
import Title from './Title';


const HorizontalList = ({title, data}) => {
    return(
        <>
        <View style={styles.listStyle}>
        <Title size={20}>{title}</Title>
        <FlatList data={data} keyExtractor={item => item.id} horizontal showsHorizontalScrollIndicator={false}
        renderItem={({item}) => <SmallerCard item={item}/>} />
        </View>
        </>
    )   
}

const styles = StyleSheet.create({
    listStyle:{
        marginVertical: 15,
    }
})


export default HorizontalList;