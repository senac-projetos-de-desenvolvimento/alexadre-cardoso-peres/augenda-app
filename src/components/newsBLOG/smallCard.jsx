import React from 'react';
import { View, StyleSheet, Dimensions } from 'react-native'
import BlockCard from './blockCard'

const {width} = Dimensions.get('window')

const SmallerCard = ({item}) => {
    return(
        <BlockCard item={item} style={styles.container} imageStyle={styles.image}/>
    )
}

const styles = StyleSheet.create({
    container:{
        width: width/2,
        marginRight: 15,
        height: 200
    },
    image:{
        height: 100
    }
})


export default SmallerCard;