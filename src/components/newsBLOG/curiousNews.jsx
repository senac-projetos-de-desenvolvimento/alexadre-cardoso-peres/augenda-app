import React from 'react';
import { View, StyleSheet } from 'react-native'
import HorizontalList from './horizontalList';


const CuriousNews = ({data}) => {
    return(
        <HorizontalList title="Curiosidades" data={data}/>
    )   
}

const styles = StyleSheet.create({
    container:{}
})


export default CuriousNews;