import React from 'react';
import { View, StyleSheet } from 'react-native'
import HorizontalList from './horizontalList';


const FunnyNews = ({data}) => {
    return(
        <HorizontalList title="Noticias" data={data}/>
    )   
}

const styles = StyleSheet.create({
    container:{}
})


export default FunnyNews;