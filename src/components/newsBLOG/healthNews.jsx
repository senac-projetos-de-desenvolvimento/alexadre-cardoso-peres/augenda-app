import React from 'react';
import { View, StyleSheet } from 'react-native'
import HorizontalList from './horizontalList';


const HealthNews = ({data}) => {
    return(
        <HorizontalList title="Cuidados" data={data}/>
    )   
}

const styles = StyleSheet.create({
    container:{}
})


export default HealthNews;