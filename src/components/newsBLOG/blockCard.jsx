import React from 'react'
import { View, StyleSheet, Image, Linking } from 'react-native'
import Title from './Title'
import Subtitle from './Subtitle'
import { TouchableOpacity } from 'react-native-gesture-handler'

const BlockCard = ({style, imageStyle, item}) => {
  const{thumbnail, title, desc, pageLink} = item

  return (
    <TouchableOpacity onPress={() => Linking.openURL(pageLink)}>
    <View style={[styles.container, style]}>
      <Image source={{uri: thumbnail}} style={[styles.image, imageStyle]}/>
        <View style={styles.contentContainer}>
          <Title>{title}</Title>
          <Subtitle>{desc}</Subtitle>
        </View>
    </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
    container:{
      width: '90%',
      height: 300,
      borderRadius: 8,
      overflow: 'hidden',
      backgroundColor: 'white',
      marginTop: 10,
      marginLeft: 20
    },
    image:{
      height: 200
    },
    contentContainer:{
      padding: 5,
    }
})

export default BlockCard;