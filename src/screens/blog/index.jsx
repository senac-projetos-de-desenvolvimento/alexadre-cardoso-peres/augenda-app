import React from 'react'
import { useNavigation } from '@react-navigation/native'
import { StyleSheet, View, Text, Image } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import CuriousNews from '../../components/newsBLOG/curiousNews';
import Featured from '../../components/newsBLOG/featuredNews'
import HealthNews from '../../components/newsBLOG/healthNews';
import data from '../../../newsAPI'
import FunnyNews from '../../components/newsBLOG/funnyNews';
import Searchbar from '../../components/newsBLOG/newsSearchbar';



export const Blog = () => {
    const navigation = useNavigation()
    const healthNews = data.filter(item => item.category === 'cuidados-list')
    const funnyNews = data.filter(item => item.category === 'noticias-list')
    const curiousNews = data.filter(item => item.category === 'curiosidades-list')

    
    return(
        
    <ScrollView style={styles.container}>

        <Searchbar/>

       <Featured item={{
        id: "0",
        title: "Agropecuaria Entre Rios",
        desc:
          "Trabalhamos com uma linha completa de farmácia veterinária, correaria, rações, pecuária e muito mais.",
        thumbnail: "https://d3ugyf2ht6aenh.cloudfront.net/stores/001/144/837/themes/bahia/1-slide-1601483591870-1617158371-e8192975e58ecab4e542ea089a1de2ad1601483571-1920-1920.webp?931126168",
        category: "ads",
        pageLink: "https://loja.agropecuariaentrerios.com.br/"
       }}/>

       <HealthNews data={healthNews}/>
       <FunnyNews data={funnyNews}/>
       <CuriousNews data={curiousNews}/>
    </ScrollView>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#7DC9DE',
    }
})