import React, { useEffect, useState } from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, View, Text, TextInput, Image, TouchableOpacity } from 'react-native';
import { auth } from '../../config/firebaseconfig';
import { signInWithEmailAndPassword, GoogleAuthProvider, signInWithPopup, FacebookAuthProvider} from 'firebase/auth'
import { useNavigation } from '@react-navigation/native';
import loginLogo from '../../../assets/loginLogo.png'
import { AntDesign } from '@expo/vector-icons'

const providerGoogle = new GoogleAuthProvider()
const providerFacebook = new FacebookAuthProvider()

export function UserLogin ()  {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const navigation = useNavigation()

    useEffect(() => {
      auth.onAuthStateChanged(user =>{
        if (user) {
          navigation.navigate('Home')
        }
      })
    })

      const loginUser = async () => {
        try{
        await signInWithEmailAndPassword(auth, email, password)
        navigation.navigate('Home')
      } catch (error){
        console.log(error.message)
        alert(error.message)
      }
    };

    const loginFacebook = () => {
      signInWithPopup(auth, providerFacebook)
      .then((re) => {
        console.log(re)
      })
      .catch((err) => {
        console.log(err.message)
      })
    }

    function loginGoogle() {
      signInWithPopup(auth, providerGoogle)
      .then((result) => {
        const credential = GoogleAuthProvider.credentialFromResult(result)
        const token = credential.accessToken
        const user = result.user

        console.log(token)
        console.log(user)
      })
      .catch((error) => {
        const errorCode = error.code
        const errorMessage = error.message
        const email = error.email
        const credential = GoogleAuthProvider.credentialFromError(error)
      })
    }
        
  return (
    <View style={styles.container}>
        <Image style={styles.login} source={loginLogo}/>
      <StatusBar style="auto" />
      <TextInput
      placeholder="Email"
      placeholderTextColor="#CAE699"
      value={email}
      keyboardType='email-address'
      autoCapitalize='none'
      autoCorrect={false}
      onChangeText={value => setEmail(value)}
      style={styles.inputTest}
      />
      <TextInput
      placeholder="Senha"
      placeholderTextColor="#CAE699"
      value={password}
      autoCapitalize='none'
      autoCorrect={false}
      onChangeText={value => setPassword(value)}
      style={[styles.inputTest, {marginBottom: 10}]}
      secureTextEntry
      />
    
      <View style={styles.button}>
        <TouchableOpacity
          style={styles.buttonLogin}
          onPress={() => loginUser()}>
          <Text style={styles.buttonTextoLogin}>Login</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.buttonRegister}
          onPress={() => navigation.navigate('UserRegister')}>
          <Text style={[styles.buttonTextoRegister]}>Registrar-se</Text>
        </TouchableOpacity>
      </View>


      <View style={styles.authButtons}>
        <View>
      <TouchableOpacity
          style={styles.buttonRegisterGoogle}
          onPress={() => loginGoogle()}>
          <AntDesign name="google" size={24} color="white" />
        </TouchableOpacity>
        </View>

        <View>
      <TouchableOpacity
          style={styles.buttonRegisterFacebook}
          onPress={() => loginFacebook()}>
          <AntDesign name="facebook-square" size={24} color="white" />
        </TouchableOpacity>
        </View>

        </View>
    </View>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#CAE699',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputTest:{
      backgroundColor: 'white',
      paddingHorizontal: 15,
      paddingVertical: 10,
      borderRadius: 10,
      marginTop: 10,
      width: '70%',
      height: 50,
  },
  buttonRegister:{
    width: '100%',
    marginTop: 10,
    backgroundColor: '#CAE699',
    padding: 15,
    borderRadius: 10,
    borderColor: 'white',
    borderWidth: 2,
    textAlign: 'center'
  },
  buttonLogin:{
    width: '100%',
    marginTop: 10,
    backgroundColor: 'white',
    padding: 15,
    borderRadius: 10,
    paddingHorizontal: 75,
    textAlign: 'center'
  },
  buttonTextoRegister:{
    width: '100%',
    fontSize: 15,
    lineHeight: 21,
    fontWeight: 'bold',
    letterSpacing: 0.25,
    color: 'black',
    textAlign: 'center'

  },
  buttonTextoLogin:{
    fontSize: 15,
    lineHeight: 21,
    fontWeight: 'bold',
    letterSpacing: 0.25,
    color: 'black',
    textAlign: 'center'
  },
  login:{
    height: 356,
    width: 356,
    padding: 5
  },
  button:{
    width: '60%',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center'
  },
  buttonRegisterGoogle:{
    width: '100%',
    marginTop: 10,
    marginLeft: 50,
    left: 20,
    backgroundColor: '#ff8c8a',
    padding: 15,
    borderRadius: 10,
    borderColor: 'white',
    borderWidth: 5,
    textAlign: 'center'
  },
  buttonRegisterFacebook:{
    width: '100%',
    margin: -65,
    marginLeft: -70,
    backgroundColor: '#8aabff',
    padding: 15,
    borderRadius: 10,
    borderColor: 'white',
    borderWidth: 5,
    textAlign: 'center'
  },
});
