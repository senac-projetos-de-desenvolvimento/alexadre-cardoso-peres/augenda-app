import { StyleSheet, View, Text, TouchableOpacity, FlatList, TextInput, Image } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { AntDesign, FontAwesome  } from '@expo/vector-icons';
import { useEffect, useState } from 'react';
import { auth, dbPet } from '../../config/firebaseconfig';
import { onValue, ref } from 'firebase/database';
import Card from '../../components/Card';




export const Home = () => {

  let recognition = null;
  let SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition
  if(SpeechRecognition !== undefined){
    recognition = new SpeechRecognition
  }

  const handleMicClick = () => {
      if(recognition !== null){
        recognition.onstart = () => {
          setListening(true)
        }
        recognition.onend = () => {
          setListening(false)
        }
        recognition.onresult = (e) => {
          setSearchText( e.results[0][0].transcript.toLowerCase())
          console.log( e.results[0][0].transcript.toLowerCase())
        }
        recognition.start()
      }
  }

  const [listening, setListening] = useState(false)
  const [pets, setPets] = useState([])
  const [filterPets, setFilterPets] = useState([])
  const navigation = useNavigation()


  //Read pets cadastrados
  useEffect(() => {
    auth.onAuthStateChanged((user) =>{
      if(user){
        onValue(ref(dbPet, `/${auth.currentUser.uid}`), (snapshot) =>{
          setPets([])
          setFilterPets([])
          const data = snapshot.val()
          if (data !== null){
            Object.values(data).map((pet) =>{
              setPets((oldArray) => [...oldArray, pet])
              setFilterPets((oldArray) => [...oldArray, pet])
            })
          }
        })
      }
    })
  }, [])

  //Seta a função de busca
  function setSearchText(s){
    let arr = JSON.parse(JSON.stringify(filterPets).toLowerCase())
    arr.filter
    setPets(arr.filter((d) => d.nome.includes(s) || d.idade.includes(s) || d.raça.includes(s)))
  }

  return (
    <View style={styles.container}>

        <View style={styles.searchArea}>

        <TextInput
          style={styles.input}
          placeholder="Pesquise por um pet"
          placeholderTextColor="#888"
          onChangeText={(s) => setSearchText(s)}
          />

        <TouchableOpacity style={styles.buttonMic} onPress={handleMicClick}>
        <FontAwesome name="microphone" size={22}
        style={{ color: listening ? '#ad7266' : "white" }}/>
        </TouchableOpacity>

        
        </View>
        <TouchableOpacity
          style={[styles.button, styles.menu]}
          onPress={ () => navigation.navigate('PetRegister')}>
          <AntDesign name="plus" size={24} color="black" />
        </TouchableOpacity>
        

          <FlatList
          data={pets}
          showsHorizontalScrollIndicator={false}
          renderItem={({ item }) => (
            <TouchableOpacity onPress={() => navigation.navigate('ReviewDetails', item)}>
            <Card>
              <Text style={styles.textTitle}>{ item.nome }</Text>
              <hr></hr>
              <Text style={styles.textCard}>{['Raça:  '+ item.raça]}</Text>
              <Text style={styles.textCard}>{['Nascimento:  ' + item.idade  ]}</Text>
            </Card>
            </TouchableOpacity>
          )}/>

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#7DC9DE',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button:{
    position: 'absolute',
    width: 60,
    height: 60,
    borderRadius: 60/2,
    justifyContent: 'center',
    alignItems: 'center',
    shadowRadius: 10,
    shadowColor: '#00213B',
    shadowOpacity: 0.3,
    shadowOffset: { height: 10, },
    bottom: 30,
    right: 20,
  },
  menu: {
    backgroundColor: 'white'  
  },
  textCard:{
    fontSize: 15,
    color: 'Black',
    textAlign: 'center'
  },
  textTitle:{
    fontSize: 15,
    fontWeight: 'bold',
    color: 'Black',
    textAlign: 'center'
  },
  input:{
    height: 35,
    width: 400,
    borderColor: 'white',
    borderWidth: 3,
    marginBottom: 20,
    marginTop: 20,
    borderRadius: 10,
    paddingLeft: 15,
  },
  buttonMic:{
    position: 'absolute',
    bottom: 25,
    right: 15,
  },
});
