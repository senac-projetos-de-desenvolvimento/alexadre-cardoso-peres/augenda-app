import React from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, View, Text, Pressable, ImageBackground } from 'react-native';
import { auth } from '../../config/firebaseconfig';
import { useNavigation } from '@react-navigation/native';


export function User () {
  const navigation = useNavigation()

  const logOut = async () => {
    auth
    .signOut()
    .then(() =>{
      navigation.replace('UserLogin')
    })
  }
  

  return (
    <View style={styles.container}>
      <ImageBackground  source={require('../../../assets/backScreen.png')} style={{width: '100%', height: '100%'}}>

      <Text style={styles.texto}>{auth.currentUser?.email}</Text>
      
      <StatusBar style="auto" />
      <View style={styles.button}>
        <Pressable style={styles.buttonButton} onPress={logOut}>
          <Text style={styles.buttonTexto}>Deslogar-se</Text>
        </Pressable>
      </View>
      
      </ImageBackground>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#7DC9DE',
  },
  texto:{
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white',
    marginTop: 550,
    marginLeft: '5%',
    marginRight: '5%',
    textAlign: 'center',
    borderColor: 'white',
    borderWidth: 3,
    paddingHorizontal: 30,
    paddingVertical: 10,
    borderRadius: 15,
    textShadowColor:'#585858',
    textShadowOffset:{width: 5, height: 5},
    textShadowRadius:10,
  },
  buttonButton:{
    width: '100%',
    height: '40%',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 100,
    backgroundColor: 'white',
    padding: 30,
    borderRadius: 50,
  },
  buttonTexto:{
    fontSize: 20,
    lineHeight: 21,
    fontWeight: 'bold',
    letterSpacing: 0.25,
    color: 'black',
  }
});
