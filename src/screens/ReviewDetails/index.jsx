import React, { useRef } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Image } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import { ref, remove } from 'firebase/database';
import { auth, dbPet } from '../../config/firebaseconfig';
import { Modalize } from 'react-native-modalize'
import * as Print from 'expo-print'
import Card from '../../components/Card';
import CardBack from '../../components/CardBack';
import AugendaLogo from '../../../assets/AugendaLogo.png'
import gerarPdf from '../../../assets/gerarPdf.png'




export default function ReviewDetails({route, navigation }){
  const { nome } = route.params
  const { raça } = route.params
  const { idade } = route.params
  const { pelos } = route.params
  const { vacina } = route.params
  const { doencas } = route.params
  const { notes } = route.params

  const modalizeRef = useRef(null)
  function onOpen(){
    modalizeRef.current?.open()
  }

  const modalizePdf = useRef(null)
  function onOpenPdf(){
    modalizePdf.current?.open()
  }


   const print = async () => {
    await Print.printAsync({
      html: createDynamicTable(),
      margins: {
        left: 20,
        top: 50,
        right: 20,
        bottom: 100,
      },
      height: 100
      
    })
   }
   


   const createDynamicTable = () => {
    var table = ''
    table = table + `
      <tr>
        <td>${nome}</td>
        <td>${raça}</td>
        <td>${idade}</td>
        <td>${pelos}</td>
      </tr>
      `

    console.log(table)
   }

    //Deleta pets cadastrados
    const handleDelete = (uid) => {
      const { uidd } = route.params
      remove(ref(dbPet, `/${auth.currentUser.uid}/${uidd}`))
    }


  return (
    <View style={styles.container}>


      
        <CardBack>

        <Image style={styles.logo} source={AugendaLogo}/>

        <Card>
        <Text style={styles.text}>Nome do pet</Text>
        <Text style={styles.textInto}>{nome}</Text>
        </Card>

        <Card>
        <Text style={styles.text}>Raça do pet</Text>
        <Text style={styles.textInto}>{raça}</Text>
        </Card>

        <Card>
        <Text style={styles.text}>Data de nascimento do pet</Text>
        <Text style={styles.textInto}>{idade}</Text>
        </Card>

        <Card>
        <Text style={styles.text}>Pelagem/Cor do pet</Text>
        <Text style={styles.textInto}>{pelos}</Text>
        </Card>
        
        </CardBack>
        
        <TouchableOpacity
          style={[styles.button, styles.buttonHome]}
          onPress={ () => {navigation.navigate('Home')}}>
          <AntDesign name="home" size={24} color="black" />
        </TouchableOpacity>

        <TouchableOpacity
          style={[styles.button, styles.buttonDelete]}
          onPress={ () => {handleDelete(); navigation.navigate('Home')}}>
          <AntDesign name="delete" size={24} color="black" />
        </TouchableOpacity>
        
        <TouchableOpacity
          style={[styles.button, styles.buttonVacina]}
          onPress={onOpen}>
          <AntDesign name="medicinebox" size={24} color="black" />
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.buttonPdf}
          onPress={onOpenPdf}>
          <AntDesign name="pdffile1" size={24} color="black" />
        </TouchableOpacity>


        <Modalize ref={modalizeRef} snapPoint={400}> 
          <View style={styles.subContainer}>
            <Card>
            <Text style={styles.text}>Vacinas e Medicamentos</Text>
            <Text style={styles.textInto}>{vacina}</Text>
            </Card>

            <Card>
            <Text style={styles.text}>Comorbidades</Text>
            <Text style={styles.textInto}>{doencas}</Text>
            </Card>

            <Card>
            <Text style={styles.text}>Infos Gerais</Text>
            <Text style={styles.textInto}>{notes}</Text>
            </Card>
          </View>
        </Modalize>



        <Modalize ref={modalizePdf} snapPoint={1500}> 
          <View style={styles.subContainer}>

          <Text style={styles.textPdf}>Informações do seu pet</Text>
            
            <Card>
            <Text style={styles.text}>Nome do seu pet</Text>
            <Text style={styles.textInto}>{nome}</Text>
            <br/>
            <Text style={styles.text}>Idade do seu pet</Text>
            <Text style={styles.textInto}>{idade}</Text>
            <br/>
            <Text style={styles.text}>Raça do seu pet</Text>
            <Text style={styles.textInto}>{raça}</Text>
            <br/>
            <Text style={styles.text}>Tipo de pelagem</Text>
            <Text style={styles.textInto}>{pelos}</Text>
            <br/>
            <Text style={styles.text}>Vacinas e Medicamentos</Text>
            <Text style={styles.textInto}>{vacina}</Text>
            <br/>
            <Text style={styles.text}>Comorbidades</Text>
            <Text style={styles.textInto}>{doencas}</Text>
            <br/>
            <Text style={styles.text}>Infos Gerais</Text>
            <Text style={styles.textInto}>{notes}</Text>
            </Card>

          </View>

          <TouchableOpacity
          style={[styles.buttonSave]}
          onPress={print}>
             <Image style={styles.pdfImage} source={gerarPdf}/>
        </TouchableOpacity>


        </Modalize>


    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#7DC9DE',
    alignItems: 'center',
    justifyContent: 'center',
  },
  subContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  button:{
    position: 'absolute',
    width: 100,
    height: 60,
    borderRadius: 60/2,
    justifyContent: 'center',
    alignItems: 'center',
    shadowRadius: 10,
    shadowColor: '#00213B',
    shadowOpacity: 0.5,
    shadowOffset: { height: 10, },
  },
  buttonHome:{
    backgroundColor: 'white',
    shadowRadius: 10,
    shadowColor: '#00213B',
    shadowOpacity: 0.5,
    marginLeft: 250,
    shadowOffset: { height: 10, },
    top: 750,
  },
  buttonDelete:{
    marginRight: 250,
    backgroundColor: '#FAAAB1',
    shadowRadius: 10,
    shadowColor: '#00213B',
    shadowOpacity: 0.5,
    shadowOffset: { height: 10, },
    top: 750,
  },
  buttonVacina:{
    backgroundColor: '#faf9a7',
    shadowRadius: 10,
    shadowColor: '#00213B',
    shadowOpacity: 0.5,
    shadowOffset: { height: 10, },
    top: 750,
  },
  text: {
    fontSize: 15,
    fontWeight: 'bold',
    color: 'Black',
    textAlign: 'center',
  },
  textInto: {
    fontSize: 15,
    color: 'Black',
    textAlign: 'center',
  },
  logo:{
    height: 150,
    width: 150,
    borderRadius: 400,
    marginLeft: 80
  },
  buttonPdf:{
    backgroundColor: '#B1A581',
    shadowRadius: 10,
    shadowColor: '#00213B',
    shadowOpacity: 0.5,
    shadowOffset: { height: 10, },
    top: 20,
    left: 15,
    position: 'absolute',
    width: 35,
    height: 30,
    borderRadius: 60/2,
    justifyContent: 'center',
    alignItems: 'center',
    shadowRadius: 10,
    shadowColor: '#00213B',
    shadowOpacity: 0.5,
    shadowOffset: { height: 10, },
  },
  buttonSave:{
    backgroundColor: '#9CD89C',
    shadowRadius: 10,
    shadowColor: '#A17F43',
    shadowOpacity: 0.5,
    shadowOffset: { height: 10, },
    top: 785,
    left: 225,
    position: 'absolute',
    width: 10,
    height: 10,
    borderRadius: 60/2,
    justifyContent: 'center',
    alignItems: 'center',
    shadowRadius: 10,
    shadowColor: '#00213B',
    shadowOpacity: 0.5,
    shadowOffset: { height: 10, },
  },
  pdfImage:{
    height: 150,
    width: 300,
  },
  textPdf: {
    fontSize: 30,
    fontWeight: 'bold',
    color: 'Black',
    textAlign: 'center',
  },
});
