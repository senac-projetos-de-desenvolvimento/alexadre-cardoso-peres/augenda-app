import React, { useState, useEffect, useRef } from 'react'
import { StatusBar } from 'expo-status-bar'
import { StyleSheet, View, Text, TextInput, TouchableOpacity, Platform } from 'react-native'
import { useNavigation } from '@react-navigation/native'
import { uid } from 'uid'
import { dbPet, auth } from '../../config/firebaseconfig'
import { set, ref as dbRef } from 'firebase/database'
import { AntDesign } from '@expo/vector-icons'
import { storage } from '../../config/firebaseconfig'
import { getDownloadURL, ref, uploadBytes } from 'firebase/storage'
import { Modalize } from 'react-native-modalize'
import Avatar from '@mui/material/Avatar'
import * as ImagePicker from 'expo-image-picker'

export const PetRegister = () => {
    const modalizeRef = useRef(null)
    function onOpen(){
      modalizeRef.current?.open()
    }

    const [image, setImage] = useState(null)
    const [url, setUrl] = useState(null)
    const [nome, setNome] = useState('')
    const [idade, setIdade] = useState('')
    const [raça, setRaça] = useState('')
    const [pelos, setPelos] = useState('')
    const [vacina, setVacina] = useState('')
    const [doencas, setDoencas] = useState('')
    const [notes, setNotes] = useState('')
    const navigation = useNavigation()

    const uidd = uid();

    //Request para acessar dados do celular
    useEffect( async () => {
      if (Platform.android !== 'web'){
        const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync()
        if (status !== 'granted'){
          alert('Permissão negada')
        }
      }
    }, [])

    //Request para escrever dados na realtimeDatabase do firebase
    const writeToDatabase = () => {
      set(dbRef(dbPet, `/${auth.currentUser.uid}/${uidd}`), {
        nome: nome,
        idade: idade,
        raça: raça,
        pelos: pelos,
        vacina, vacina,
        doencas, doencas,
        notes,notes,
        uidd: uidd,
      });
    }

  //Fazer a troca de imagem avatar
  const handleImageChange = (e) => {
    if (e.target.files[0]){
      setImage(e.target.files[0])
    }
  };
  
  //Submit na imagem para o firebase
  const handleSubmit = () => {
    const imageRef = ref(storage, `/${auth.currentUser.uid}/${image.name}`)
    uploadBytes(imageRef, image)
    .then(() => {
      getDownloadURL(imageRef)
      .then((url) => {
        setUrl(url)
      })
      .catch((error) =>{
        console.log(error.message, "Url da imagem errada")
      })
      setImage(null)
    })
    .catch((error) => {
      console.log(error.message)
    })

  }

  return (
    <View style={styles.container}>
      <StatusBar style="auto" />

    <View>
          <Avatar src={url} sx={{width: 150, height: 150, marginLeft: 19, marginBottom: 3}}/>
             <button style={{position: 'absolute', width: 50, height: 50, marginLeft: 255, marginTop: 5, borderRadius: 50/2, backgroundColor:'white'}}>
              <i>
                <AntDesign name="camera" size={24} color="#7DC9DE"/>
              </i>
             </button>
             <input 
             type="file"
             onChange={handleImageChange}
             style={{position: 'relative', width: 50, height: 50, left: 255, bottom: 170, opacity: 0}}
             />
              <button onClick={handleSubmit} style={{position: 'absolute', width: 150, height: 50, marginLeft: 152, marginTop: 170, backgroundColor:'white', borderRadius: 20, fontWeight: 'bold'}}>Carregar Imagem</button>
    </View>

      <View style={styles.dados}>

        <Text style={styles.texto}>Nome</Text>
        <TextInput
          style={styles.campNormal}
          type="text"
          value={nome}
          placeholder="Pantufa"
          onChange={(nome) => setNome(nome.target.value)}
        />

        <Text style={styles.texto}>Nascimento</Text>
        <TextInput
          style={styles.campNormal}
          type="text"
          value={idade}
          placeholder="28/11/2002"
          onChange={(idade) => setIdade(idade.target.value)}
        />

        <Text style={styles.texto}>Raça</Text>
        <TextInput
          style={styles.campNormal}
          type="text"
          value={raça}
          placeholder="Shih-Tsu"
          onChange={(raça) => setRaça(raça.target.value)}
        />

        <Text style={styles.texto}>Cor - Pelagem</Text>
        <TextInput
          style={styles.campNormal}
          type="text"
          value={pelos}
          placeholder="Preto e branco. Longos e lisos."
          onChange={(pelos) => setPelos(pelos.target.value)}
        />
        
        <TouchableOpacity
          style={[styles.button, styles.menu, styles.buttonAccept]}
          onPress={onOpen}>
          <AntDesign name="plus" size={24} color="#7DC9DE" />
        </TouchableOpacity>
        
        <TouchableOpacity
          style={[styles.button, styles.menu, styles.buttonHome]}
          onPress={ () => navigation.navigate('Home')}>
          <AntDesign name="home" size={24} color="#7DC9DE" />
        </TouchableOpacity>
      </View>




      <Modalize ref={modalizeRef} snapPoint={800}>

      <Text style={styles.vacText}>Informações</Text>

      <View style={styles.vacCamps}>
        <TextInput
          style={styles.vacCamp}
          type="vacina"
          value={vacina}
          multiline="true"
          placeholder="Informe aqui medicamentos e vacinas."
          onChange={(vacina) => setVacina(vacina.target.value)}
        />

        <TextInput
          style={styles.vacCamp}
          type="doencas"
          value={doencas}
          multiline="true"
          placeholder="Informe aqui alergias, doenças e comorbidades."
          onChange={(doencas) => setDoencas(doencas.target.value)}
        />

        <TextInput
          style={styles.vacCamp}
          type="notes"
          value={notes}
          multiline="true"
          placeholder="Coloque aqui informações gerais sobre o pet. Raçoes, petiscos, banhos. Dentre outros..."
          onChange={(notes) => setNotes(notes.target.value)}
        />
        

        <View style={{flex:1, height: 180,  flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
              <TouchableOpacity
                style={[styles.buttonVacAdd, styles.drawerMenu]}
                onPress={ () => {writeToDatabase(); navigation.navigate('Home')}}>
                <Text style={styles.textoVac}>Confirmar Cadastro Pet</Text>
              </TouchableOpacity>
        </View>
      </View> 
      </Modalize>

    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#7DC9DE',
    justifyContent: 'center',
  },
  texto: {
    fontSize: 25,
    fontWeight: 'bold',
    color: '#FFF',
    marginLeft: 14,
  },
  vacText: {
    fontSize: 25,
    fontWeight: 'bold',
    color: '#0C2931',
    marginLeft: 165,
    marginTop: 15
  },
  campNormal: {
    height: 55,
    width: '100%',
    borderColor: 'white',
    borderWidth: 3,
    marginBottom: 20,
    borderRadius: 20,
    paddingLeft: 14,  
  },
    vacCamp: {
    height: 150,
    width: '100%',
    borderColor: '#0C2931',
    borderWidth: 3,
    marginBottom: 20,
    borderRadius: 20,
    paddingLeft: 14,  
    width: 435,
    marginLeft: 15,
  },
  dados:{
    marginBottom: 70,
    marginLeft: 20,
    marginRight: 20
  },
  button:{
    position: 'absolute',
    width: 60,
    height: 60,
    borderRadius: 60/2,
    justifyContent: 'center',
    alignItems: 'center',
    shadowRadius: 10,
    shadowColor: '#00213B',
    shadowOpacity: 0.3,
    shadowOffset: { height: 10, },
    top: 465,
  },
  buttonAccept:{
    marginLeft: 100
  },
  buttonHome:{
    marginLeft: 270
  },
  menu: {
      backgroundColor: 'white'
  },
  drawerMenu: {
    backgroundColor: '#0C2931'
},
  fileInput:{
    marginLeft: 5,
    marginBottom: 5
  },
  vacCamps:{
    marginTop: 50
  },
  buttonVacAdd:{
    position: 'absolute',
    width: 300,
    height: 60,
    borderRadius: 30/2,
    justifyContent: 'center',
    alignItems: 'center',
    shadowRadius: 10,
    shadowColor: '#00213B',
    shadowOpacity: 0.3,
    shadowOffset: { height: 10, },
    top: 30,
  },
  textoVac: {
    fontSize: 25,
    fontWeight: 'bold',
    color: '#FFF',
  },
})
