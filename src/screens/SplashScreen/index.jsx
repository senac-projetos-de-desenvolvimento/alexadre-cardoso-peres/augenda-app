import React, { useEffect } from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Image } from 'react-native';
import AugendaLogo from '../../../assets/AugendaLogo.png'

export const SplashScreen = ({ navigation }) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.navigate('UserLogin')
    }, 5000)
  }, [navigation])

  return (
    <View style={styles.container}>
      <Image style={styles.logo} source={AugendaLogo}/>
      <Text style={styles.texto}>Augenda</Text>
      <Text style={styles.descri}>Agenda de acompanhamento pet</Text>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#7DC9DE',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo:{
    height: 256,
    width: 256,
    borderRadius: 400,
    
  },
  texto:{
    fontSize: 35,
    fontWeight: 'bold',
    color: '#FFF',
    marginLeft: 5
  },
  descri:{
    fontSize: 15,
    fontWeight: 'bold',
    color: '#FFF',
    marginLeft: 5
  }
});
