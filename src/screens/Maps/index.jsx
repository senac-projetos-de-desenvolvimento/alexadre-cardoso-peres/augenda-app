import React, { useState, useEffect } from 'react'
import { StatusBar } from 'expo-status-bar'
import { StyleSheet, View, Text, TextInput, TouchableOpacity, Platform } from 'react-native'
import { useNavigation } from '@react-navigation/native'

export const Maps = () => {
    const navigation = useNavigation()


    return(
        <View style={styles.container}>
          <iframe width="100%" height="100%" id="gmap_canvas" src="https://maps.google.com/maps?q=Pet&t=&z=13&ie=UTF8&iwloc=&output=embed"/>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#7DC9DE',
        alignItems: 'center',
        justifyContent: 'center',
    }
})