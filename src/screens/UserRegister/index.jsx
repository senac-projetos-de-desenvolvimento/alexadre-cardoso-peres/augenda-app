import React, { useEffect, useState } from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, View, Text, TextInput, Image, TouchableOpacity } from 'react-native';
import { auth } from '../../config/firebaseconfig';
import { createUserWithEmailAndPassword, signInWithEmailAndPassword } from 'firebase/auth'
import { useNavigation } from '@react-navigation/native';
import registroLogo from '../../../assets/registroLogo.png'


export function UserRegister ()  {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const navigation = useNavigation()

    useEffect(() => {
      auth.onAuthStateChanged(user =>{
        if (user) {
          navigation.navigate('Home')
        }
      })
    })

    const createUser = async () => {
      try{
      await createUserWithEmailAndPassword(auth, email, password)
      alert('Cadastro Feito com sucesso.')
    } catch (error){
      console.log(error.message)
      alert(error.message)
    }

  };

      const loginUser = async () => {
        try{
        await signInWithEmailAndPassword(auth, email, password)
        navigation.navigate('Home')
      } catch (error){
        console.log(error.message)
        alert(error.message)
      }
    };
        
  return (
    <View style={styles.container}>
        <Image style={styles.login} source={registroLogo}/>
      <StatusBar style="auto" />
      <TextInput
      placeholder="Email"
      placeholderTextColor="#313131"
      value={email}
      keyboardType='email-address'
      autoCapitalize='none'
      autoCorrect={false}
      onChangeText={value => setEmail(value)}
      style={styles.inputTest}
      />
      <TextInput
      placeholder="Senha"
      placeholderTextColor="#313131"
      value={password}
      autoCapitalize='none'
      autoCorrect={false}
      onChangeText={value => setPassword(value)}
      style={[styles.inputTest, {marginBottom: 10}]}
      secureTextEntry
      />

      <View style={styles.button}>
        <TouchableOpacity
          style={styles.buttonRegister}
          onPress={() => loginUser()}>
          <Text style={styles.buttonTextoRegister}>Login</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.buttonLogin}
          onPress={() => createUser()}>
          <Text style={[styles.buttonTextoLogin, styles.buttonOutline]}>Registrar-se</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F0BE88',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputTest:{
      backgroundColor: 'white',
      paddingHorizontal: 15,
      paddingVertical: 10,
      borderRadius: 10,
      marginTop: 10,
      width: '70%',
      height: 50,
  },
  buttonRegister:{
    width: '100%',
    marginTop: 10,
    backgroundColor: '#F0BE88',
    padding: 15,
    borderRadius: 10,
    borderColor: 'white',
    borderWidth: 2,
    textAlign: 'center'
  },
  buttonLogin:{
    width: '100%',
    marginTop: 10,
    backgroundColor: 'white',
    padding: 15,
    borderRadius: 10,
    paddingHorizontal: 75,
    textAlign: 'center'
  },
  buttonTextoRegister:{
    width: '100%',
    fontSize: 15,
    lineHeight: 21,
    fontWeight: 'bold',
    letterSpacing: 0.25,
    color: 'black',
    textAlign: 'center'
  },
  buttonTextoLogin:{
    fontSize: 15,
    lineHeight: 21,
    fontWeight: 'bold',
    letterSpacing: 0.25,
    color: 'black',
  },
  login:{
    height: 356,
    width: 356,
    padding: 5
  },
  button:{
    width: '60%',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center'
  }
});
