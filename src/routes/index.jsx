import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Home, SplashScreen, User, PetRegister, UserLogin, UserRegister, Maps, Blog} from '../screens';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import ReviewDetails from '../screens/ReviewDetails';
import Ionicons from 'react-native-vector-icons/Ionicons'


const BottomRoute = () =>{
    const Tab = createBottomTabNavigator()

    return(
        <Tab.Navigator screenOptions={({ route }) => ({ 
            headerShown: false,
            tabBarShowLabel: false,
            tabBarStyle: {
                height: 75,
               
            },
            tabBarIcon: ({ focused, color, size }) => {
              let iconName;
  
              if (route.name === 'Pet') {
                iconName = focused
                  ? 'paw-outline'
                  : 'paw-outline';
                  

              } else if (route.name === 'User') {
                iconName = focused 
                  ? 'person-outline' 
                  : 'person-outline';

              } else if (route.name === 'Maps') {
                iconName = focused 
                  ? 'md-map-outline' 
                  : 'md-map-outline';

              } else if (route.name === 'Blog') {
                iconName = focused 
                  ? 'earth-outline' 
                  : 'earth-outline';

              }

              
              

              return <Ionicons name={iconName} size={35} color={color} />;
            },
            tabBarActiveTintColor: '#7DC9DE',
            tabBarInactiveTintColor: '#0C2931',
          })}
        >
          <Tab.Screen name="Pet" component={Home} />
          <Tab.Screen name="User" component={User} />
          <Tab.Screen name="Maps" component={Maps}/>
          <Tab.Screen name="Blog" component={Blog}/>
        </Tab.Navigator>
    )
}


export const Routes = () =>{
    const Stack = createNativeStackNavigator()

    return(
        <NavigationContainer>
            <Stack.Navigator screenOptions={{ headerShown: false }}>
                <Stack.Screen name="Splash" component={SplashScreen}/>
                <Stack.Screen name="ReviewDetails" component={ReviewDetails}/>
                <Stack.Screen name="UserLogin" component={UserLogin}/>
                <Stack.Screen name="UserRegister" component={UserRegister}/>
                <Stack.Screen name="PetRegister" component={PetRegister}/>
                <Stack.Screen name="Home" component={BottomRoute}/>
                <Stack.Screen name="User" component={BottomRoute}/>
                <Stack.Screen name="Maps" component={BottomRoute}/>
                <Stack.Screen name="Blog" component={BottomRoute}/>
            </Stack.Navigator>
        </NavigationContainer>
    )
}