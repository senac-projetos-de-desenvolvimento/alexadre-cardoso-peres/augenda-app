import firebase from "firebase/app";
import { getAuth } from 'firebase/auth';
import { initializeApp } from 'firebase/app';
import { getFirestore } from "firebase/firestore"
import { getDatabase } from "firebase/database";
import { getStorage } from "firebase/storage";
import "firebase/storage"

const firebaseConfig = {
  apiKey: "AIzaSyDpmeITlw_VVMdOqGBCptut20MCkd2fKic",
  authDomain: "authentication-89282.firebaseapp.com",
  databaseURL: "https://authentication-89282-default-rtdb.firebaseio.com",
  projectId: "authentication-89282",
  storageBucket: "authentication-89282.appspot.com",
  messagingSenderId: "409884865007",
  appId: "1:409884865007:web:db06289567015d5abb061a"
};

const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);
export const dbPet = getDatabase(app);
export const auth = getAuth(app);
export const storage = getStorage(app);