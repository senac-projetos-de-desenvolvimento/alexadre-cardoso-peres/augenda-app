export default [
    {
      id: "1",
      title: "Kit primeiros socorros",
      desc:
        "Aprenda a fazer um kit de primeiros socorros para seu pet",
      thumbnail: "https://petindica.com.br/wp-content/uploads/2022/05/o-que-incluir-no-kit-de-primeiros-socorros-de-seu-pet.jpg",
      category: "cuidados-list",
      pageLink: "https://www.ative.pet/bem-estar/caes-com-medo-de-fogos/"
    },
    {
      id: "2",
      title: "Dicas para ajudar pets com medo de fogos",
      desc:
        "Aprenda como ajudar seu animalzinho a se proteger dos fogos",
      thumbnail: "https://uploads.metropoles.com/wp-content/uploads/2019/12/30121749/fogos1.jpg",
      category: "cuidados-list",
      pageLink: "https://www.ative.pet/bem-estar/caes-com-medo-de-fogos/"
    },
    {
      id: "3",
      title: "Como cuidar filhotes",
      desc:
        "Seu gatinho ou cãozinho teve filhotinhos? Aprenda como cuidar-los",
      thumbnail: "https://nsc-total-wp.s3.sa-east-1.amazonaws.com/wp-content/uploads/2022/10/Cuidados-com-filhotes-Shutterstock.jpg",
      category: "cuidados-list",
      pageLink: "https://www.pulsoseguros.com.br/blog/7-cuidados-para-ter-com-filhotes-de-cachorros-e-gatos-em-casa/"
    },
    {
      id: "4",
      title: "Quais são os principais cuidados com a saúde dos pets?",
      desc:
        "Saúde animal: dicas importantes para garantir o bem-estar dos bichos de estimação",
      thumbnail: "https://p2.trrsf.com/image/fget/cf/648/0/images.terra.com/2022/05/19/1351331029-gripeempetsachegadadofriopedecuidadoscomasaude.jpg",
      category: "cuidados-list",
      pageLink: "https://www.imexmedicalgroup.com.br/blog/quais-sao-os-principais-cuidados-com-a-saude-dos-pets/"
    },
    {
      id: "5",
      title: "Numero de animais abandonados cresce",
      desc:
        "No brasil os numeros de animais de rua crescem cada vez mais",
      thumbnail: "https://www.amoviralata.com/wp-content/uploads/2020/10/animal-de-rua.jpg",
      category: "noticias-list",
      pageLink: "https://g1.globo.com/df/distrito-federal/df2/video/cresce-o-numero-de-animais-abandonados-11140978.ghtml"
    },
    {
      id: "6",
      title: "Feira de adoção em belem",
      desc:
        "Feira de adoção de animais em belem do pará",
      thumbnail: "https://s2.glbimg.com/P9yfZNUU0QZ_t4xKo7zKqol6Y8A=/0x0:850x560/984x0/smart/filters:strip_icc()/i.s3.glbimg.com/v1/AUTH_59edd422c0c84a879bd37670ae4f538a/internal_photos/bs/2022/h/P/1rm6AYRAqJUaTZ9T5BCg/caesegatos.jpg",
      category: "noticias-list",
      pageLink: "https://g1.globo.com/pa/para/noticia/2022/11/18/belem-realiza-feira-de-adocao-de-caes-e-gatos-neste-sabado-19.ghtml"
    },
    {
      id: "7",
      title: "Como reagir ao encontrar animais silvestres",
      desc:
        "Como deve-se reagir ao encontrar animais silvestres",
      thumbnail: "https://www.worldanimalprotection.org.br/sites/default/files/styles/1200x630/public/media/selvagemexoticosilvestre_png.png?itok=pRlwe5_Y",
      category: "noticias-list",
      pageLink: "https://globoplay.globo.com/v/11133428/"
    },
    {
      id: "8",
      title: "Rastreador de araras",
      desc:
        "Araras tem mutirão para fazer microchipagem de animais para reduzir abandono",
      thumbnail: "https://s2.glbimg.com/4_FS5Kg4M3U-_x7EXeY3P1yu-bc=/0x0:1000x562/984x0/smart/filters:strip_icc()/i.s3.glbimg.com/v1/AUTH_59edd422c0c84a879bd37670ae4f538a/internal_photos/bs/2022/B/C/2OVguYTMKIOGUIfc1tXw/chip.jpg",
      category: "noticias-list",
      pageLink: "https://g1.globo.com/sp/sao-carlos-regiao/noticia/2022/11/17/araras-tem-mutirao-para-fazer-microchipagem-de-animais-para-reduzir-abandono.ghtml"
    },
    {
      id: "9",
      title: "Donos procuram por animais desaparecidos",
      desc:
        "Donos de todos os cantos procuram por seus pets desaparecidos",
      thumbnail: "https://blog.cobasi.com.br/wp-content/uploads/2021/05/vira-lata-caramelo-principal.jpg",
      category: "noticias-list",
      pageLink: "https://globoplay.globo.com/v/11120329/"
    },
    {
      id: "10",
      title: "Pai que não queria cachorro ganha cachorro shih-tzu ",
      desc:
        "Veja sua reação",
      thumbnail: "https://fotos.amomeupet.org/uploads/fotos/660x0_1668795976_6377ce489cf4f_hd.webp",
      category: "curiosidades-list",
      pageLink: "https://www.amomeupet.org/noticias/7199/pai-que-nao-queria-cachorro-ganha-cachorro-shih-tzu--e-essa-foi-a-sua-reacao-video"
    },
    {
      id: "11",
      title: "Casal vende até suas alianças",
      desc:
        "Casal vende até suas alianças para poder cuidar de 130 gatos resgatados em sua casa",
      thumbnail: "https://fotos.amomeupet.org/uploads/fotos/660x0_1668702298_6376605a8c935_hd.webp",
      category: "curiosidades-list",
      pageLink: "https://www.amomeupet.org/noticias/7194/casal-vende-ate-suas-aliancas-para-poder-cuidar-de-130-gatos-resgatados-em-sua-casa"
    },
    {
      id: "12",
      title: "Macaco faz amizade com cachorro",
      desc:
        "Macaco faz amizade com cachorro e o leva para passear na floresta",
      thumbnail: "https://fotos.amomeupet.org/uploads/fotos/660x0_1668627200_63753b000d70e_hd.webp",
      category: "curiosidades-list",
      pageLink: "https://www.amomeupet.org/noticias/7191/macaco-faz-amizade-com-cachorro-e-o-leva-para-passear-na-floresta"
    },
    {
      id: "13",
      title: "Pastor alemão salva",
      desc:
        "Em vídeo impressionante, pastor alemão salva criança de ataque de outro cachorro",
      thumbnail: "https://fotos.amomeupet.org/uploads/fotos/660x0_1668613124_63750404284d8_hd.webp",
      category: "curiosidades-list",
      pageLink: "https://www.amomeupet.org/noticias/7188/em-video-impressionante-pastor-alemao-salva-crianca-de-ataque-de-outro-cachorro"
    },
    {
      id: "14",
      title: "Cícero, o cão salsicha",
      desc:
        "Conheça Cícero, o cão salsicha que possui mais profissões que a Barbie; vídeos",
      thumbnail: "https://fotos.amomeupet.org/uploads/fotos/660x0_1668370511_6371504fbe6a7_hd.webp",
      category: "curiosidades-list",
      pageLink: "https://www.amomeupet.org/noticias/7177/conheca-cicero-o-cao-salsicha-que-possui-mais-profissoes-que-a-barbie-videos"
    },
  ];
  